Gem::Specification.new do |s|
  s.name        = 'ruby_vault'
  s.version     = '0.4.0'
  s.description = 'Package for accessing vault through ruby'
  s.summary     = 'Vault for Rocketmakers'
  s.authors     = ['developers@rocketmakers.com']
  s.files       = ['lib/ruby_vault.rb']
  s.license       = 'MIT'
  s.require_paths = ['lib']
  s.add_dependency 'vault'
end
