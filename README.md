## vault_ruby
Package for accessing vault through ruby

#### gemfile
Add this to your gemfile
```
gem 'ruby_vault', :git => 'https://gitlab.com/rocketmakers/ruby-vault.git', :tag => '0.1.0'
```

#### install gem
Run this during installation
```
gem install bundler --no-rdoc --no-ri
bundle install
```

#### Vault connection
The connection to vault will use your local session by default, if VAULT_ROLE_ID and VAULT_SECRET_ID are present these will be used instead. VAULT_ADDR is also used as the connection address.

#### get secret in ruby
```
require 'rubygems'
require 'bundler/setup'
require 'ruby_vault'

client = RubyVault.login()
secret_value = RubyVault.get_secret(client, "project", "path/to/key", :"key")
```

#### save secret to file
```
require 'rubygems'
require 'bundler/setup'
require 'ruby_vault'

client = RubyVault.login()
file_path = RubyVault.save_secret_to_file(client, "project", "path/to/key", :"key")
# remove file after it has been used
```