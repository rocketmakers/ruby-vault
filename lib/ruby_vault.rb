#!/usr/bin/env ruby

require 'vault'
class RubyVault
  def self.login(token_environment_name = nil)
    token = nil
    if ENV['VAULT_ROLE_ID'] && ENV['VAULT_SECRET_ID']
      role_id = ENV['VAULT_ROLE_ID']
      secret_id = ENV['VAULT_SECRET_ID']

      token = Vault.auth.approle(
        role_id,
        secret_id
      )

      unless token_environment_name.nil?
        ENV[token_environment_name] = token.auth.client_token
      end
    end

    Vault::Client.new(
      address: ENV['VAULT_ADDR'],
      token: token
    )
  end

  def self.get_secret(client, project, secret_path, value)
    # Renew the token so we don't time out if we're using vault
    Vault.auth_token.renew_self

    project = if project == 'rocketmakers'
                project + '/data'
              else
                'projects/data/' + project
              end

    full_path = secret_path == '' ? project : project + '/' + secret_path

    client.with_retries(Vault::HTTPConnectionError, Vault::HTTPError) do |a, e|
      if e
        puts "Received exception from Vault - attempt #{a}"
        raise e
      else
        puts "Getting secret #{full_path} #{value}"
        secret = Vault.logical.read(full_path)
        return secret.data[:data][value]
      end
    end
  end

  def self.save_secret_to_file(client, project, secret_path, value)
    secret = get_secret(client, project, secret_path, value)
    temp_path = "#{Dir.home}/.#{project}-#{value}"

    puts "Writing temporary file: #{temp_path}"
    File.open(temp_path, 'w') do |f|
      f.write(secret)
    end

    temp_path
  end
end
